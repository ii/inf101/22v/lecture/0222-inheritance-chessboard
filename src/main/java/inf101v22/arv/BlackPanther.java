package inf101v22.arv;

public class BlackPanther extends Mammal {

    BlackPanther(String name) {
        super(name);
    }

    @Override
    String getMammalType() {
        return "black panther";
    }

    @Override
    String getNoise() {
        return "roar";
    }
}
